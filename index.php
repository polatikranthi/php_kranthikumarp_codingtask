﻿<?php
include('includes/db_config.php');
extract($_REQUEST);
$products_id = 1;
$sql="SELECT * FROM `tbl_pro_detailes` WHERE `id`='$products_id'";

if ($result= mysqli_query($con,$sql))
  {
    if($row = mysqli_fetch_array($result)) {
    $username = $row["productname"];
    $or_price = $row["orginalPrice"];
    $offer_price = $row["offerPrice"];
    $seller = $row["seller"];
    $image = $row["image"];
   
  }
}
$spe_sql="SELECT * FROM `tbl_pro_spec` WHERE `prod_id`='$products_id'";

if ($spe_result= mysqli_query($con,$spe_sql))
  {
    if($row = mysqli_fetch_array($spe_result)) {
    $content = $row["content"];
    $warranty = $row["warranty"];
    $highlights = $row["highlights"];
    $m_number = $row["m_number"];
    $m_name = $row["m_name"];
    $color = $row["color"];
    $simtype = $row["simtype"];
    $touchscreen = $row["touchscreen"];
    $display_size = $row["display_size"];
    $resolution = $row["resolution"];
    $operating_sys = $row["operating_sys"];
    $processor_core = $row["processor_core"];
    $in_storage = $row["in_storage"];
    $ram = $row["ram"];
    $pr_camera = $row["pr_camera"];
    $sc_camera = $row["sc_camera"];
    $network_type = $row["network_type"];
    $battery = $row["battery"];
    
    // $high_des = explode(",", $highlights);
    // echo $high_des;
   
  }
}






?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->

  <meta name="author" content="Mobile">

<title>Mobile World</title>

<!-- Favicons Icon -->
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS Style -->
<link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="product-page">
<!-- Mobile Menu -->
<div id="jtv-mobile-menu">
  <ul>
    <li>
      <div class="mm-search">
        <form id="mob-search" name="search">
          <div class="input-group">
            <div class="input-group-btn">
              <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
          </div>
        </form>
      </div>
    </li>
    <li><a href="index.php">Home</a>
     
    </li>
    <li><a href="#">Pages</a>
     
    </li>
    <li><a href="#">Mobiles</a>
      
    </li>
    <li><a href="#">Blog</a></li>
    <li><a href="#">Contact Us</a></li>
  </ul>
  <div class="top-links">
    <ul class="links">
      <li><a title="My Account" href="#">My Account</a></li>
      <li><a title="Wishlist" href="#">Wishlist</a></li>
      <li><a title="Checkout" href="#">Checkout</a></li>
      <li><a title="Blog" href="#"><span>Blog</span></a></li>
      <li class="last"><a title="Login" href="#"><span>Login</span></a></li>
    </ul>
    <div class="language-box">
      <select class="selectpicker" data-width="fit">
        <option>English</option>
        <option>Francais</option>
        <option>German</option>
        <option>Español</option>
      </select>
    </div>
    <div class="currency-box">
      <form class="form-inline">
        <div class="input-group">
          <div class="currency-addon">
            <select class="currency-selector">
              <option data-symbol="$">USD</option>
              <option data-symbol="€">EUR</option>
              <option data-symbol="£">GBP</option>
              <option data-symbol="¥">JPY</option>
              <option data-symbol="$">CAD</option>
              <option data-symbol="$">AUD</option>
            </select>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="page"> 
  <!-- Header -->
 <?php
 include_once("header.php");
 ?>
  <!-- end header --> 
 
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a href="index.php" title="Go to Home Page">Home</a> <span>/</span></li>
            <li><a href="#" title="">Mobile</a> <span>/ </span></li>
           
            <li> <strong><?= $username?> </strong> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="container">
      <div class="row">
        <div class="product-view">
          <div class="product-essential">
            <form action="#" method="post" id="product_addtocart_form">
              <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                <div class="product-image">
                  <div class="product-full">
                    <div class="new-label new-top-left"> New </div>
                    <img id="product-zoom" src="images/products/<?=$image?>" data-zoom-image="images/products/<?=$image?>" alt="product-image"/> </div>
                  <div class="more-views">
                    <div class="slider-items-products">
                      <div id="jtv-more-views-img" class="product-flexslider hidden-buttons product-img-thumb">
                        <div class="slider-items slider-width-col4 block-content">
                          <?php 
                          $img_sql="SELECT * FROM `tbl_pro_images` WHERE `pro_id`='$products_id'";

                          if ($result= mysqli_query($con,$img_sql)){
                           
                            while($row=mysqli_fetch_array($result))
                            { 
                             $image_name = $row["imag_name"]; ?>
                              <div class="more-views-items"> <a href="#" data-image="images/products/<?=$image_name?>" data-zoom-image="images/products/<?=$image_name?>"> <img id="product-zoom"  src="images/products/<?=$image_name?>" alt="product-image"/> </a></div>
                            <?php }
                          } ?>
                         
                        <!--   <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom"  src="images/products/product-fashion-1.jpg" alt="product-image"/> </a></div>
                          <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom"  src="images/products/product-fashion-1.jpg" alt="product-image"/> </a></div>
                          <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom"  src="images/products/product-fashion-1.jpg" alt="product-image"/> </a> </div>
                          <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom"  src="images/products/product-fashion-1.jpg" alt="product-image" /> </a></div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end: more-images --> 
              </div>
              <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                <div class="product-name">
                  <h1><?= $username?></h1>
                </div>
                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                <div class="price-block">
                  <div class="price-box">
                    <p class="special-price"> <span class="price-label">Special Price</span><span class="price">&#x20b9; <?= $offer_price?> </span></p>
                    <p class="old-price"> <span class="price-label">Regular Price:</span><span class="price"> &#x20b9;<?= $or_price?> </span></p>
                    <p class="availability in-stock"><span>In Stock</span></p>
                  </div>
                </div>
                <div class="short-description">
                  <h2>Quick Overview</h2>
                  <p><?= $content ?></p>
                  
                </div>
                 <div class="short-description">
                  <h2>Warrenty</h2>
                  <p><?= $warranty ?></p>
                  
                </div>
                <div class="add-to-box">
                  <div class="add-to-cart">
                    <div class="pull-left">
                      <div class="custom pull-left">
                        <label>Quantity:</label>
                        <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                        <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                        <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                      </div>
                    </div>
                    <button onClick="productAddToCartForm.submit(this)" class="button btn-cart" title="Add to Cart" type="button"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                  <div class="email-addto-box">
                    <ul class="add-to-links">
                      <li><a class="link-wishlist" href="#"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
                      <li><a class="link-compare" href="#"><i class="fa fa-signal"></i><span>Add to Compare</span></a></li>
                      <li><a class="email-friend" href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
                    </ul>
                  </div>
                </div>
                <div class="social">
                  <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                </div>
                <div class="product-next-prev"> <a class="product-next" href="#"><i class="fa fa-angle-left"></i></a> <a class="product-prev" href="#"><i class="fa fa-angle-right"></i></a> </div>
              </div>
            </form>
          </div>
        </div>
        <div class="product-collateral col-lg-12 col-sm-12 col-xs-12">
          <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
            <li class="active"> <a href="#product_tabs_description" data-toggle="tab"> Product Description </a></li>
            <li><a href="#product_tabs_tags" data-toggle="tab">Highlights</a></li>
            
            <li><a href="#product_tabs_custom" data-toggle="tab">Specifications</a></li>
            <li><a href="#reviews_tabs" data-toggle="tab">Reviews</a></li>
          </ul>
          <div id="productTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="product_tabs_description">
              <div class="std">
                <p><?=$content?></p>
               
              </div>
            </div>
            <div class="tab-pane fade" id="product_tabs_tags">
              <div class="box-collateral box-tags">
                <div class="tags">
                  <form id="addTagForm" action="#" method="get">
                    <div class="form-add-tags">
                      <label for="productTagName">Highlights:</label>

                     <p><?= $highlights ?><p>
                      <!--input-box--> 
                    </div>
                  </form>
                </div>
                <!--tags-->
              
              </div>
            </div>
            <div class="tab-pane fade" id="reviews_tabs">
              <div class="box-collateral box-reviews" id="customer-reviews">
                <div class="box-reviews1">
                  <div class="form-add">
                    <form id="review-form" method="post" action="#">
                      <h3>Write Your Own Review</h3>
                      <fieldset>
                        <h4>How do you rate this product? <em class="required">*</em></h4>
                        <span id="input-message-box"></span>
                        <table id="product-review-table" class="data-table">
                          <thead>
                            <tr class="first last">
                              <th>&nbsp;</th>
                              <th><span class="nobr">1 *</span></th>
                              <th><span class="nobr">2 *</span></th>
                              <th><span class="nobr">3 *</span></th>
                              <th><span class="nobr">4 *</span></th>
                              <th><span class="nobr">5 *</span></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="first odd">
                              <th>Price</th>
                              <td class="value"><input type="radio" class="radio" value="11" id="Price_1" name="ratings[3]"></td>
                              <td class="value"><input type="radio" class="radio" value="12" id="Price_2" name="ratings[3]"></td>
                              <td class="value"><input type="radio" class="radio" value="13" id="Price_3" name="ratings[3]"></td>
                              <td class="value"><input type="radio" class="radio" value="14" id="Price_4" name="ratings[3]"></td>
                              <td class="value last"><input type="radio" class="radio" value="15" id="Price_5" name="ratings[3]"></td>
                            </tr>
                            <tr class="even">
                              <th>Value</th>
                              <td class="value"><input type="radio" class="radio" value="6" id="Value_1" name="ratings[2]"></td>
                              <td class="value"><input type="radio" class="radio" value="7" id="Value_2" name="ratings[2]"></td>
                              <td class="value"><input type="radio" class="radio" value="8" id="Value_3" name="ratings[2]"></td>
                              <td class="value"><input type="radio" class="radio" value="9" id="Value_4" name="ratings[2]"></td>
                              <td class="value last"><input type="radio" class="radio" value="10" id="Value_5" name="ratings[2]"></td>
                            </tr>
                            <tr class="last odd">
                              <th>Quality</th>
                              <td class="value"><input type="radio" class="radio" value="1" id="Quality_1" name="ratings[1]"></td>
                              <td class="value"><input type="radio" class="radio" value="2" id="Quality_2" name="ratings[1]"></td>
                              <td class="value"><input type="radio" class="radio" value="3" id="Quality_3" name="ratings[1]"></td>
                              <td class="value"><input type="radio" class="radio" value="4" id="Quality_4" name="ratings[1]"></td>
                              <td class="value last"><input type="radio" class="radio" value="5" id="Quality_5" name="ratings[1]"></td>
                            </tr>
                          </tbody>
                        </table>
                        <input type="hidden" value="" class="validate-rating" name="validate_rating">
                        <div class="review1">
                          <ul class="form-list">
                            <li>
                              <label class="required" for="nickname_field">Nickname<em>*</em></label>
                              <div class="input-box">
                                <input type="text" class="input-text" id="nickname_field" name="nickname">
                              </div>
                            </li>
                            <li>
                              <label class="required" for="summary_field">Summary<em>*</em></label>
                              <div class="input-box">
                                <input type="text" class="input-text" id="summary_field" name="title">
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="review2">
                          <ul>
                            <li>
                              <label class="required " for="review_field">Review<em>*</em></label>
                              <div class="input-box">
                                <textarea rows="3" cols="5" id="review_field" name="detail"></textarea>
                              </div>
                            </li>
                          </ul>
                          <div class="buttons-set">
                            <button class="button submit" title="Submit Review" type="submit"><span>Submit Review</span></button>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                <div class="box-reviews2">
                  <h3>Customer Reviews</h3>
                  <div class="box visible">
                    <ul>
                      <li>
                        <table class="ratings-table">
                          <tbody>
                            <tr>
                              <th>Value</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Quality</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Price</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="review">
                          <h6><a href="#">Lorem ipsum dolor sit amet</a></h6>
                          <small>Review by <span>Sophia </span>on 15/01/2018 </small>
                          <div class="review-txt">Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante.</div>
                        </div>
                      </li>
                      <li class="even">
                        <table class="ratings-table">
                          <tbody>
                            <tr>
                              <th>Value</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Quality</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Price</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="review">
                          <h6><a href="#">Lorem ipsum dolor sit amet</a></h6>
                          <small>Review by <span>William</span>on 05/02/2018 </small>
                          <div class="review-txt">Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero.</div>
                        </div>
                      </li>
                      <li>
                        <table class="ratings-table">
                          <tbody>
                            <tr>
                              <th>Value</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Quality</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                            <tr>
                              <th>Price</th>
                              <td><div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div></td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="review">
                          <h6><a href="#">Lorem ipsum dolor sit amet</a></h6>
                          <small>Review by <span> Mason</span>on 10/02/2018 </small>
                          <div class="review-txt last">Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper.</div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="actions"> <a class="button view-all" id="revies-button" href="#"><span><span>View all</span></span></a> </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="product_tabs_custom">
              <div class="form-add-tags">
                <label for="productTagName">Gentral</label>
     
                  <p> <strong>Model Number</strong> : <?= $m_number ?></p>
                  <p> <strong>Model Name</strong> : <?= $m_name ?></p>
                  <p> <strong>Color</strong> : <?= $color ?></p>
                  <p> <strong>Sim Type</strong> : <?= $color ?></p>
                  <p> <strong>Touchscreen</strong> : <?= $touchscreen ?></p>
                  

                   <label for="productTagName">Display Features</label>
                   <p> <strong>Display Size</strong> : <?= $display_size ?></p>
                  <p> <strong>Resolution</strong> : <?= $resolution ?></p>
                  <label for="productTagName">Os & Processor Features</label>
                   <p> <strong>Operating System</strong> : <?= $operating_sys ?></p>
                  <p> <strong>Processor Core</strong> : <?= $processor_core ?></p>
                   <label for="productTagName">Memory & Storage Features</label>
                   <p> <strong>Internal Storage</strong> : <?= $in_storage ?></p>
                  <p> <strong>RAM </strong> : <?= $ram ?></p>
                  <label for="productTagName">Camera Features</label>
                   <p> <strong>Primary Camera</strong> : <?= $pr_camera ?></p>
                  <p> <strong>Secondary Camera </strong> : <?= $sc_camera ?></p>
                   <label for="productTagName">Connectivity Features</label>
                   <p> <strong>Network Type</strong> : <?= $network_type ?></p>
                 
                   <label for="productTagName">Battery & Power Features</label>
                   <p> <strong>Battery Capacity</strong> : <?= $battery ?></p>
                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
  
  <!-- Related Products Slider -->
  
  <div class="container">
    <div class="jtv-related-products">
      <div class="slider-items-products">
        <div class="jtv-new-title">
          <h2>Related Products</h2>
        </div>
        <div class="related-block">
          <div id="jtv-related-products-slider" class="product-flexslider hidden-buttons">
            <div class="slider-items slider-width-col4 products-grid">
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="new-label new-top-left">new</div>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href="#"><span>Quick View</span></a> <a href="#">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="#">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 115.00</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="sale-label sale-top-right">Sale</div>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href=""><span>Quick View</span></a> <a href="wishlist.html">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 99.00</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href=""><span>Quick View</span></a> <a href="">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 79.90</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="sale-label sale-top-left">Sale</div>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href=""><span>Quick View</span></a> <a href="">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 49.00</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href=""><span>Quick View</span></a> <a href="">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 97.00</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-inner">
                  <div class="item-img">
                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                      <div class="mask-shop-white"></div>
                      <div class="new-label new-top-left">new</div>
                      <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                      <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                      </a> <a href="compare.html">
                      <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                      </a> </div>
                  </div>
                  <div class="item-info">
                    <div class="info-inner">
                      <div class="item-title"> <a title="Product tilte is here" href="#"> Product tilte is here </a> </div>
                      <div class="item-content">
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                        <div class="item-price">
                          <div class="price-box"> <span class="regular-price"> <span class="price">&#x20b9; 199.00</span></span></div>
                        </div>
                        <div class="actions">
                          <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Related Products Slider End --> 
  
  <!-- Upsell Product Slider -->
  
<?php mysqli_close($con);

      include_once("footer.php");
  ?>
</div>

<!-- JavaScript --> 
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/main.js"></script> 
<script src="js/jquery.flexslider.js"></script> 
<script src="js/owl.carousel.min.js"></script> 
<script src="js/mob-menu.js"></script> 
<script src="js/cloud-zoom.js"></script>
</body>
</html>