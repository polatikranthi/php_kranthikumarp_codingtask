 <header>
    <div class="header-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-3 col-xs-12"> 
            
            <!-- Header Logo -->
            <div class="logo"><a title="ecommerce Template" href="index.html"><span style="color: #fff; font-size: 20px;">Mobile World</span></a></div>
            <!-- End Header Logo -->
            <div class="nav-icon">
              <div class="mega-container visible-lg visible-md visible-sm">
                <div class="navleft-container">
                  <div class="mega-menu-title">
                    <h3><i class="fa fa-navicon"></i>Categories</h3>
                  </div>
                  <div class="mega-menu-category">
                    <ul class="nav">
                      <li><a href="#">Home</a>
                        <div class="wrap-popup column1">
                          <div class="popup">
                            <ul class="nav">
                             
                            </ul>
                          </div>
                        </div>
                      </li>
                     
                     
                   
                     
                      <li class="nosub"><a href="#">Accessories</a></li>
                      <li><a href="#">Blog</a>
                       
                      </li>
                      <li class="nosub"><a href="#">Contact Us</a></li>
                    </ul>
                    <div class="side-banner"><img src="images/top-banner.jpg" alt="Flash Sale" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-9 col-sm-9 col-xs-12 jtv-rhs-header">
            <div class="jtv-mob-toggle-wrap">
              <div class="mm-toggle"><i class="fa fa-reorder"></i><span class="mm-label">Menu</span></div>
            </div>
            <div class="jtv-header-box">
              <div class="search_cart_block">
                <div class="search-box hidden-xs">
                  <form id="search_mini_form" action="#" method="get">
                    <input id="search" type="text" name="q" value="" class="searchbox" placeholder="Search entire store here..." maxlength="128">
                    <button type="submit" title="Search" class="search-btn-bg" id="submit-button"><span class="hidden-sm">Search</span><i class="fa fa-search hidden-xs hidden-lg hidden-md"></i></button>
                  </form>
                </div>
                <div class="right_menu">
                  <div class="menu_top">
                    <div class="top-cart-contain pull-right">
                      <div class="mini-cart">
                        <div class="basket"><a class="basket-icon" href="#"><i class="fa fa-shopping-basket"></i> Shopping Cart <span>0</span></a>
                          <div class="top-cart-content">
                            <div class="block-subtitle">
                              <div class="top-subtotal">0 items, <span class="price">$00.00</span></div>
                            </div>
                          
                            <div class="actions"> <a href="#" class="view-cart"><span>View Cart</span></a>
                              <button onclick="window.location.href='#'" class="btn-checkout" title="Checkout" type="button"><span>Checkout</span></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Header Language -->
                  <div class="language-box hidden-xs">
                    <select class="selectpicker" data-width="fit">
                      <option>English</option>
                      <option>Francais</option>
                      <option>German</option>
                      <option>Español</option>
                    </select>
                  </div>
                  <div class="currency-box hidden-xs">
                    <form class="form-inline">
                      <div class="input-group">
                        <div class="currency-addon">
                          <select class="currency-selector">
                            <option data-symbol="$">USD</option>
                            <option data-symbol="€">EUR</option>
                            <option data-symbol="£">GBP</option>
                            <option data-symbol="¥">JPY</option>
                            <option data-symbol="$">CAD</option>
                            <option data-symbol="$">AUD</option>
                          </select>
                        </div>
                      </div>
                    </form>
                  </div>
                  
                  <!-- End Header Currency --> 
                </div>
              </div>
              <div class="top_section hidden-xs">
                <div class="toplinks">
                  <div class="site-dir hidden-xs"> <a class="hidden-sm" href="#"><i class="fa fa-phone"></i> <strong>Hotline:</strong> +1 123 456 7890</a> <a href="#"><i class="fa fa-envelope"></i> support@example.com</a> </div>
                  <ul class="links">
                    <li><a title="My Account" href="#">My Account</a></li>
                    <li><a title="My Wishlist" href="#">Wishlist</a></li>
                    <li><a title="Checkout" href="#">Checkout</a></li>
                    <li><a title="Track Order" href="#">Track Order</a></li>
                    <li><a title="Log In" href="#">Log In</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>