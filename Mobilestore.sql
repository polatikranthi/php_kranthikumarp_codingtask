-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 25, 2018 at 01:00 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Mobilestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pro_detailes`
--

CREATE TABLE `tbl_pro_detailes` (
  `id` int(11) NOT NULL,
  `productname` varchar(150) NOT NULL,
  `orginalPrice` varchar(10) NOT NULL,
  `offerPrice` varchar(10) NOT NULL,
  `image` varchar(50) NOT NULL,
  `seller` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pro_detailes`
--

INSERT INTO `tbl_pro_detailes` (`id`, `productname`, `orginalPrice`, `offerPrice`, `image`, `seller`) VALUES
(1, 'Samsung Galaxy On6 (Blue, 64 GB)  (4 GB RAM)', '25,500', '20,990', 'samsung-galaxy-1.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pro_images`
--

CREATE TABLE `tbl_pro_images` (
  `id` int(11) NOT NULL,
  `pro_id` int(10) NOT NULL,
  `imag_name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pro_images`
--

INSERT INTO `tbl_pro_images` (`id`, `pro_id`, `imag_name`, `status`) VALUES
(1, 1, 'samsung-galaxy-1.jpg', 0),
(2, 1, 'samsung-galaxy-2.jpeg', 0),
(4, 1, 'samsung-galaxy-4.jpeg', 0),
(5, 1, 'samsung-galaxy-5.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pro_spec`
--

CREATE TABLE `tbl_pro_spec` (
  `id` int(11) NOT NULL,
  `prod_id` int(10) NOT NULL,
  `content` varchar(500) NOT NULL,
  `warranty` varchar(150) NOT NULL,
  `highlights` varchar(500) NOT NULL,
  `m_number` varchar(150) NOT NULL,
  `m_name` varchar(100) NOT NULL,
  `color` varchar(10) NOT NULL,
  `simtype` varchar(10) NOT NULL,
  `touchscreen` varchar(10) NOT NULL,
  `display_size` varchar(150) NOT NULL,
  `resolution` varchar(100) NOT NULL,
  `operating_sys` varchar(100) NOT NULL,
  `processor_core` varchar(50) NOT NULL,
  `in_storage` varchar(100) NOT NULL,
  `ram` varchar(10) NOT NULL,
  `pr_camera` varchar(10) NOT NULL,
  `sc_camera` varchar(10) NOT NULL,
  `network_type` varchar(50) NOT NULL,
  `battery` varchar(150) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pro_spec`
--

INSERT INTO `tbl_pro_spec` (`id`, `prod_id`, `content`, `warranty`, `highlights`, `m_number`, `m_name`, `color`, `simtype`, `touchscreen`, `display_size`, `resolution`, `operating_sys`, `processor_core`, `in_storage`, `ram`, `pr_camera`, `sc_camera`, `network_type`, `battery`, `status`) VALUES
(1, 1, 'Samsung Galaxy A6 64GB was launched in May 2018 & runs on Android 8 OS. The Smartphone is available in more than three color options i.e.Black, Gold, Blue, and Lavender & has a built in fingerprint sensor as the primary security feature, along with the host of connectivity options in terms of 3G, 4G, Wifi Bluetooth capabilities. The phone is available with 64GB of internal storage.', 'Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories', '4 GB RAM | 64 GB ROM |,\r\n5.6 inch Display,\r\n16MP Rear Camera | 16MP Front Camera,\r\n3000 mAh Battery', 'SM-A600GZKHINS', 'Galaxy A6', 'Black', 'Dual Sim', 'YES', '5.6 inch', '720 x 1480 Pixels', 'Android Oreo 8.0', 'Octa Core', '\r\n64 GB', ' 4 GB', '16MP', '16MP', '4G VoLTE, GSM, 4G LTE, WCDMA', '\r\n3000 mAh', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phonenumber` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pro_detailes`
--
ALTER TABLE `tbl_pro_detailes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pro_images`
--
ALTER TABLE `tbl_pro_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pro_spec`
--
ALTER TABLE `tbl_pro_spec`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pro_detailes`
--
ALTER TABLE `tbl_pro_detailes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pro_images`
--
ALTER TABLE `tbl_pro_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_pro_spec`
--
ALTER TABLE `tbl_pro_spec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
